import React, { Component } from 'react';
import './App.css';
import rest from 'rest';

const ReactDOM = require('react-dom');

class App extends Component {

    componentWillMount() {
        this.setState({movies: []});
    }

    componentDidMount() {
        rest({method: 'GET',
              path: 'http://localhost:8080/api/movie/all',
              headers: {
                'Access-Control-Allow-Origin' : '*',
                'Access-Control-Allow-Methods': '*'
            }}).done(response => {
            this.setState({movies: JSON.parse(response.entity) });
        });
    }

    render() {
        return (
            <div>
                <CreateDialog onCreate={this.onCreate}/>
                <MovieList movies={this.state.movies}/>
            </div>
    	)
    }
}

class MovieList extends Component{
    render() {
        var movies = this.props.movies.map(movie =>
            <Movie key={movie.id} movie={movie}/>
    );
    return (
        <table>
            <tbody>
            <tr>
                <th>Title</th>
                <th>Main Actor</th>
                <th>Category</th>
                <th>Release Date</th>
                <th>Preview</th>
                <th/>
                <th/>
                <th/>
            </tr>
            {movies}
            </tbody>
        </table>
    )
    }
}

class Movie extends Component{
    constructor(props) {
        super(props);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete() {
        rest({method: 'DELETE', path: "http://localhost:8080/api/movie?id=" + this.props.movie.id}).done(response => {
            document.location.reload(true); // fix me.
        });
    }

    render() {
        var releaseDate = new Date(this.props.movie.releaseDate * 1000).toGMTString();
        var previewUrl = 'http://localhost:8080/api/movie/preview?id=' + this.props.movie.id;
        return (
            <tr>
                <td>{this.props.movie.title}</td>
                <td>{this.props.movie.mainActor}</td>
                <td><Category categoryId={this.props.movie.category}/></td>
                <td>{releaseDate}</td>
                <td>
                    <img id="myImg" name="myImg" src={previewUrl} height="128"/>
                </td>
                <td>
                    <button onClick={this.handleDelete}>Delete</button>
                </td>
                <td>
                    <UpdateDialog movie={this.props.movie}
                                  attributes={this.props.attributes}
                                  onUpdate={this.props.onUpdate}/>
                </td>
                <td>
                    <ImageUploader movieId={this.props.movie.id} />
                </td>
            </tr>
    )
    }
}

class UpdateDialog extends Component {

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    onUpdate(Movie, updatedMovie) {
        console.log(updatedMovie);

        fetch('http://localhost:8080/api/movie/', {
            method: 'PUT',
            body: JSON.stringify(updatedMovie),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => {
            window.location.reload(true); // fix me.
        }).catch(response => {
            alert('Unable to update: ' + response);
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        var updatedMovie = {};
        updatedMovie['id'] = this.props.movie.id;
        updatedMovie['title'] = ReactDOM.findDOMNode(this.refs['title']).value.trim();
        updatedMovie['mainActor'] = ReactDOM.findDOMNode(this.refs['mainActor']).value.trim();
        updatedMovie['category'] = ReactDOM.findDOMNode(this.refs['category']).value.trim();
        updatedMovie['releaseDate'] = (new Date(ReactDOM.findDOMNode(this.refs['releaseDate']).value.trim()))
                                            .getTime()/1000;
        console.log(ReactDOM.findDOMNode(this.refs['releaseDate']));
        this.onUpdate(this.props.movie, updatedMovie);
    }

    render() {
        var dialogId = "updateMovie-" + this.props.movie.id;
        var releaseDate = new Date(this.props.movie.releaseDate * 1000).toISOString();
        console.log(releaseDate);
        var date = releaseDate.substr(0,10);
        return (
            <div key={this.props.movie.id}>
                <a href={"#" + dialogId}>Update</a>
                <div id={dialogId} className="modalDialog">
                    <div>
                        <a href="#" title="Close" className="close">X</a>

                        <h2>Update a Movie</h2>
                        <form encType='application/json'>
                            <p key={this.props.movie.id + 'title'}>
                                <input type="text" placeholder={'Title'}
                                       defaultValue={this.props.movie.title}
                                       ref={'title'} className="field" />
                            </p>
                            <p key={this.props.movie.id + 'mainActor'}>
                                <input type="text" placeholder={'Main Actor'}
                                       defaultValue={this.props.movie.mainActor}
                                       ref={'mainActor'} className="field" />
                            </p>
                            <p key={this.props.movie.id + 'category'}>
                                <input type="text" placeholder={'Category'}
                                       ref={'category'} className="field" defaultValue={this.props.movie.category}/>
                            </p>
                            <p key={this.props.movie.id + 'releaseDate'}>
                                <input type="date" placeholder={'Release Date'}
                                       defaultValue={date}
                                       ref={'releaseDate'} className="field" />
                            </p>
                            <button onClick={this.handleSubmit}>Update</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }

};

class CreateDialog extends Component {

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    onCreate(newMovie) {
        console.log(newMovie);

        fetch('http://localhost:8080/api/movie/', {
            method: 'POST',
            body: JSON.stringify(newMovie),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => {
            window.location.reload(true); // fix me.
            alert('movie added.');
        }).catch(response => {
            alert('Unable to create: ' + response);
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        var newMovie = {};
        newMovie['title'] = ReactDOM.findDOMNode(this.refs['title']).value.trim();
        newMovie['mainActor'] = ReactDOM.findDOMNode(this.refs['mainActor']).value.trim();
        newMovie['category'] = ReactDOM.findDOMNode(this.refs['category']).value.trim();
        newMovie['releaseDate'] = (new Date(ReactDOM.findDOMNode(this.refs['releaseDate']).value.trim()))
            .getTime()/1000;
        this.onCreate(newMovie);
    }

    render() {
        var dialogId = "CreateNewMovie";
        return (
            <div key="newMovie">
                <a href={"#" + dialogId}>Create</a>
                <div id={dialogId} className="modalDialog">
                    <div>
                        <a href="#" title="Close" className="close">X</a>

                        <h2>Create a Movie</h2>
                        <form encType='application/json'>
                            <p key={'newMovietitle'}>
                                <input type="text" placeholder={'Title'}
                                       defaultValue={""}
                                       ref={'title'} className="field" />
                            </p>
                            <p key={'newMoviemainActor'}>
                                <input type="text" placeholder={'Main Actor'}
                                       defaultValue={""}
                                       ref={'mainActor'} className="field" />
                            </p>
                            <p key={'newMoviecategory'}>
                                <input type="text" placeholder={'Category'}
                                       ref={'category'} className="field" />
                            </p>
                            <p key={'newMoviereleaseDate'}>
                                <input type="date" placeholder={'Release Date'}
                                       ref={'releaseDate'} className="field" />
                            </p>
                            <button onClick={this.handleSubmit}>Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }

};

class ImageUploader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            file: '',
            imagePreviewUrl: ''
        };
        this.handleImageChange = this.handleImageChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();
        var formData  = new FormData();
        formData.append("file", this.state.file);
        formData.append("id", this.props.movieId)
        fetch('http://localhost:8080/api/movie/preview', {
            method: 'PUT',
            body: formData,
        }).then(response => {
            window.location.reload(true); // fix me.
        }).catch(response => {
            alert('Unable to upload: ' + response);
        });
    }

    handleImageChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                file: file,
                imagePreviewUrl: reader.result
            });
        }

        reader.readAsDataURL(file)
    }

    render() {
        let {imagePreviewUrl} = this.state;
        let $imagePreview = null;
        if (imagePreviewUrl) {
            $imagePreview = (<img src={imagePreviewUrl} height="128"/>);
        }

        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <input type="file" onChange={this.handleImageChange} />
                    <button type="submit" onClick={this.handleSubmit}>Upload Image</button>
                </form>
                {$imagePreview}
            </div>
        )
    }

}

class Category extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categoryName: '',
        };
    }

    componentDidMount() {
        var url = 'http://localhost:8080/api/movie/category?id=' + this.props.categoryId;
        fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(response => response.json())
        .then(response => {
                this.setState({categoryName: response.name });
         }).catch(response => {
            alert('Unable to get category: ' + response);
        });
    }

    render() {
        return (<p>{this.state.categoryName}</p>);
    }
}
class SelectCategory extends React.Component {
    render() {
        return (
                <select className="select-category" defaultValue={this.props.defaultValue}>
                    <option value="0"></option>
                    <option value="1"> Action</option>
                    <option value="2"> Adventure</option>
                    <option value="3"> Animation</option>
                    <option value="4"> Biography</option>
                    <option value="5"> Comedy</option>
                    <option value="6"> Crime</option>
                    <option value="7"> Documentary</option>
                    <option value="8"> Drama</option>
                    <option value="9"> Family</option>
                    <option value="10"> Fantasy</option>
                    <option value="11"> Film noir</option>
                    <option value="12"> History</option>
                    <option value="13"> Horror</option>
                    <option value="14"> Music</option>
                    <option value="15"> Musical</option>
                    <option value="16"> Mystery</option>
                    <option value="17"> Romance</option>
                    <option value="18"> Sci-Fi</option>
                    <option value="19"> Short</option>
                    <option value="20"> Sport</option>
                    <option value="21"> SuperHero</option>
                    <option value="22"> Thriller</option>
                    <option value="23"> War</option>
                    <option value="24"> Western</option>
                </select>
                );
    }
}

export default App;
