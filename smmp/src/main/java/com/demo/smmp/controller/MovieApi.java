package com.demo.smmp.controller;

import com.demo.smmp.dto.CategoryDto;
import com.demo.smmp.dto.MovieDto;
import com.demo.smmp.entity.Movie;
import com.demo.smmp.entity.Preview;
import com.demo.smmp.enumeration.CategoryEnum;
import com.demo.smmp.repository.MovieRepository;
import com.demo.smmp.repository.PreviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@CrossOrigin("http://localhost:3000")
@RestController
@RequestMapping("/api/movie")
public class MovieApi {

	@Autowired
	private MovieRepository movieRepository;

	@Autowired
	private PreviewRepository previewRepository;

	
	@RequestMapping( method = RequestMethod.POST, consumes="application/json")
	public ResponseEntity addMovie(@Valid @RequestBody MovieDto movieDto) {
		if(movieDto.getId() != null){ // enforcing POST
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
		Movie newMovie = new Movie(movieDto.getTitle(),
				movieDto.getCategory(),
				movieDto.getReleaseDate(),
				movieDto.getMainActor());
		movieRepository.save(newMovie);
		return new ResponseEntity(HttpStatus.OK);
	}

	@RequestMapping( method = RequestMethod.PUT, consumes="application/json")
	public ResponseEntity updateMovie(@Valid @RequestBody MovieDto movieDto){
		if(movieDto.getId() == null ||
				!movieRepository.findById(movieDto.getId()).isPresent()){ // enforcing PUT
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}
		Movie oldMovie = new Movie(movieDto.getTitle(),
				movieDto.getCategory(),
				movieDto.getReleaseDate(),
				movieDto.getMainActor());
		oldMovie.setId(movieDto.getId());
		movieRepository.save(oldMovie);
		return new ResponseEntity(HttpStatus.OK);
	}

	@RequestMapping( method = RequestMethod.DELETE)
	public ResponseEntity deleteMovie(@RequestParam("id") String id) {
		movieRepository.deleteById(id);
		return new ResponseEntity(HttpStatus.OK);
	}

	@RequestMapping( method = RequestMethod.GET)
	public ResponseEntity findMovie(@RequestParam("id") String id) {
		Optional<Movie> foundMovieOpt = movieRepository.findById(id);
		if(foundMovieOpt.isPresent()) {
			return new ResponseEntity(foundMovieOpt.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public ResponseEntity allMovies() {
		List<MovieDto> listMovieDto = movieRepository.findAll().stream()
				.map(movie ->  fromMovieToMovieDto(movie))
				.collect(Collectors.toList());
		return new ResponseEntity(listMovieDto,HttpStatus.OK);
	}

	// Search for movies based on title, main actor, category and release date
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public ResponseEntity search(@RequestParam(required = false, defaultValue = "") String title,
								 @RequestParam(required = false, defaultValue = "") String mainActor,
								 @RequestParam(required = false) Integer category,
								 @RequestParam(required = false, defaultValue = "0") Long releaseDateStart,
								 @RequestParam(required = false, defaultValue = "2228469257") Long releaseDateEnd) {
		// todo: find a better way
		List<MovieDto> listMovieDto;
		if(category == null){
			listMovieDto =
					movieRepository.findByTitleContainsIgnoreCaseAndMainActorContainsIgnoreCaseAndReleaseDateBetween(
						title,
						mainActor,
						releaseDateStart,
						releaseDateEnd)
								.stream()
								.map(movie ->  fromMovieToMovieDto(movie))
								.collect(Collectors.toList());
		} else {
			listMovieDto =
					movieRepository.findByTitleContainsIgnoreCaseAndMainActorContainsIgnoreCaseAndCategoryIsAndReleaseDateBetween(
						title,
						mainActor,
						category,
						releaseDateStart,
						releaseDateEnd)
							.stream()
							.map(movie ->  fromMovieToMovieDto(movie))
							.collect(Collectors.toList());
		}
		return new ResponseEntity(listMovieDto,HttpStatus.OK);
	}

	@RequestMapping(value = "/preview", method = RequestMethod.GET)
	public ResponseEntity<byte[]> getPreview(@RequestParam("id") String id) throws IOException {
		HttpHeaders headers = new HttpHeaders();
		headers.setCacheControl(CacheControl.noCache().getHeaderValue());
		Optional<Preview> previewOpt = previewRepository.findById(id);
		if(previewOpt.isPresent()) {
			byte[] media = previewOpt.get().getImage();
			return new ResponseEntity<>(media, headers, HttpStatus.OK);
		} else {
			InputStream in = getClass().getClassLoader().getResourceAsStream("static/no_image_available.png");
			byte[] media = StreamUtils.copyToByteArray(in);
			return new ResponseEntity<>(media, headers, HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/preview", method = RequestMethod.PUT)
	public ResponseEntity updatePreview(@RequestParam("file") MultipartFile file, @RequestParam("id") String id) throws IOException {
		previewRepository.save(new Preview(id,file.getBytes()));
		return new ResponseEntity(HttpStatus.OK);
	}

	@RequestMapping(value = "/categories", method = RequestMethod.GET)
	public ResponseEntity allCategories(){
		List<CategoryDto> listCategoryDto = Arrays.asList(CategoryEnum.values()).stream()
				.map(category ->  new CategoryDto(category.getId(), category.getName()))
				.collect(Collectors.toList());
		return new ResponseEntity(listCategoryDto,HttpStatus.OK);
	}

	@RequestMapping(value = "/category", method = RequestMethod.GET)
	public ResponseEntity categoryById( @RequestParam("id") int id){
		return new ResponseEntity(new CategoryDto(id, CategoryEnum.getById(id).getName()),HttpStatus.OK);
	}

	private MovieDto fromMovieToMovieDto(Movie movie){
		return new MovieDto(movie.getId(),
				movie.getTitle(),
				movie.getCategory(),
				movie.getReleaseDate(),
				movie.getMainActor());
	}


}
