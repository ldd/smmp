package com.demo.smmp.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

public class MovieDto {

	private String id;

	@NotEmpty
	private String title;

	@Min(1)
	@Max(24)
	private int category;

	@Min(-2934748800L)
	private long releaseDate;

	@NotEmpty
	private String mainActor;

	public MovieDto(){}

	public MovieDto(String id, String title, int category, long releaseDate, String mainActor) {
		this.id = id;
		this.title = title;
		this.category = category;
		this.releaseDate = releaseDate;
		this.mainActor = mainActor;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public long getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(long releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getMainActor() {
		return mainActor;
	}

	public void setMainActor(String mainActor) {
		this.mainActor = mainActor;
	}

}
