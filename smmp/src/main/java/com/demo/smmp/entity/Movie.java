package com.demo.smmp.entity;

import org.springframework.data.annotation.Id;

public class Movie {

	@Id
	private String id;

	private String title;

	private int category;

	private long releaseDate;

	private String mainActor;

	public Movie(){}

	public Movie(String title, int category, long releaseDate, String mainActor) {
		this.title = title;
		this.category = category;
		this.releaseDate = releaseDate;
		this.mainActor = mainActor;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public long getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(long releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getMainActor() {
		return mainActor;
	}

	public void setMainActor(String mainActor) {
		this.mainActor = mainActor;
	}

	@Override
	public String toString() {
		return "Movie{" +
				"id='" + id + '\'' +
				", title='" + title + '\'' +
				", category=" + category +
				", releaseDate=" + releaseDate +
				", mainActor='" + mainActor + '\'' +
				'}';
	}

}
