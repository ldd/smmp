package com.demo.smmp.entity;

import org.springframework.data.annotation.Id;

public class Preview {

	@Id
	private String id;

	private byte[] image;

	public Preview(){}

	public Preview(String id, byte[] image) {
		this.id = id;
		this.image = image;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}
}
