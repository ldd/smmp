package com.demo.smmp.enumeration;

import java.util.HashMap;
import java.util.Map;

public enum CategoryEnum {
	ACTION(1, "Action"),
	ADVENTURE(2, "Adventure"),
	ANIMATION(3, "Animation"),
	BIOGRAPHY(4, "Biography"),
	COMEDY(5, "Comedy"),
	CRIME(6, "Crime"),
	DOCUMENTARY(7, "Documentary"),
	DRAMA(8, "Drama"),
	FAMILY(9, "Family"),
	FANTASY(10, "Fantasy"),
	FILM_NOIR(11, "Film noir"),
	HISTORY(12, "History"),
	HORROR(13, "Horror"),
	MUSIC(14, "Music"),
	MUSICAL(15, "Musical"),
	MYSTERY(16, "Mystery"),
	ROMANCE(17, "Romance"),
	SCIFI(18, "Sci-Fi"),
	SHORT(19, "Short"),
	SPORT(20, "Sport"),
	SUPERHERO(21, "SuperHero"),
	THRILLER(22, "Thriller"),
	WAR(23, "War"),
	WESTERN(24, "Western");

	private int id;
	private String name;

	CategoryEnum(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	private static final Map<Integer, CategoryEnum> byId = new HashMap<>();

	static {
		for (CategoryEnum c : CategoryEnum.values()) {
			if (byId.put(c.id, c) != null) {
				throw new IllegalArgumentException("duplicate id: " + c.id);
			}
		}
	}

	public static CategoryEnum getById(int id) {
		CategoryEnum c = byId.get(id);
		if(c != null) {
			return byId.get(id);
		}
		throw new IllegalArgumentException("cannot found id: " + c.id);
	}
}
