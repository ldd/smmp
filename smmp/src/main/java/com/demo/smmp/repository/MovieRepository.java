package com.demo.smmp.repository;

import com.demo.smmp.entity.Movie;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MovieRepository extends MongoRepository<Movie, String> {

	Movie findByTitle(String title);
	List<Movie> findByCategory(int category);

	// TODO: check if I can ignore category if no present.

	// Search for movies based on title, main actor, category and release date:
	List<Movie> findByTitleContainsIgnoreCaseAndMainActorContainsIgnoreCaseAndCategoryIsAndReleaseDateBetween
		(String title, String mainActor, Integer category, Long releaseDateStart, Long releaseDateEnd);

	List<Movie> findByTitleContainsIgnoreCaseAndMainActorContainsIgnoreCaseAndReleaseDateBetween
			(String title, String mainActor, Long releaseDateStart, Long releaseDateEnd);
}