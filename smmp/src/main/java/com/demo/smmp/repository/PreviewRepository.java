package com.demo.smmp.repository;

import com.demo.smmp.entity.Preview;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PreviewRepository extends MongoRepository<Preview, String> {
}
