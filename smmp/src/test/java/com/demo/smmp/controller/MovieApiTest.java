package com.demo.smmp.controller;

import com.demo.smmp.dto.MovieDto;
import com.demo.smmp.entity.Movie;
import com.demo.smmp.enumeration.CategoryEnum;
import com.demo.smmp.repository.MovieRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.nio.charset.Charset;

@RunWith(SpringRunner.class)
@SpringBootTest(
		properties = {"spring.data.mongodb.database=test_db"},
		webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MovieApiTest {

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private MovieRepository repository;

	@Autowired
	private ObjectMapper objectMapper;

	private MockMvc mockMvc;

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(),
			Charset.forName("utf8"));

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		// db setup
		repository.deleteAll();

		// save a couple of movies
		repository.save(new Movie("Evita", CategoryEnum.MUSICAL.getId(), 852246000L, "Madonna"));
		repository.save(new Movie("The Matrix", CategoryEnum.SCIFI.getId(),929052000L, " Keanu Reeves"));

	}

	private String jsonize(Object o) throws IOException {
		return objectMapper.writeValueAsString(o);
	}

	@Test
	public void addMovie() throws Exception {
		MovieDto movieDtoToSave = new MovieDto();
		movieDtoToSave.setTitle("Avengers: Infinity War");
		movieDtoToSave.setCategory(CategoryEnum.FANTASY.getId());
		movieDtoToSave.setMainActor("Robert Downey Jr.");
		movieDtoToSave.setReleaseDate(1524745802L);
		mockMvc.perform(MockMvcRequestBuilders.post("/api/movie")
				.contentType(contentType)
				.content(jsonize(movieDtoToSave)))
				.andExpect(MockMvcResultMatchers.status().isOk());
		Assert.assertNotNull(repository.findByTitle("Avengers: Infinity War"));
	}

	@Test
	public void updateMovie() throws Exception {
		Movie movie = repository.findByTitle("Evita");
		MovieDto movieDtoToSave = new MovieDto();
		movieDtoToSave.setId(movie.getId());
		movieDtoToSave.setTitle(movie.getTitle());
		movieDtoToSave.setCategory(CategoryEnum.MUSIC.getId()); // edited field
		movieDtoToSave.setMainActor(movie.getMainActor());
		movieDtoToSave.setReleaseDate(movie.getReleaseDate());

		mockMvc.perform(MockMvcRequestBuilders.put("/api/movie")
				.contentType(contentType)
				.content(jsonize(movieDtoToSave)))
				.andExpect(MockMvcResultMatchers.status().isOk());

		Assert.assertEquals(CategoryEnum.MUSIC.getId(),repository.findById(movie.getId()).get().getCategory());
	}

	@Test
	public void removeMovie() throws Exception {
		Movie movie = repository.findByTitle("Evita");
		mockMvc.perform(MockMvcRequestBuilders.delete("/api/movie")
				.param("id", movie.getId()))
				.andExpect(MockMvcResultMatchers.status().isOk());
		Assert.assertFalse(repository.findById(movie.getId()).isPresent());
	}

	@Test
	public void searchMovie() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/api/movie/search")
				.param("title", "Evita")
				.param("mainActor","")
				.param("releaseDateStart", "0")
				.param("releaseDateEnd", "" + System.currentTimeMillis()))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void allMovie() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/api/movie/all"))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}
}
