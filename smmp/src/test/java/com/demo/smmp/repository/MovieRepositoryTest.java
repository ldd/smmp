package com.demo.smmp.repository;

import com.demo.smmp.entity.Movie;
import com.demo.smmp.enumeration.CategoryEnum;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {
		"spring.data.mongodb.database=test_db"
})
public class MovieRepositoryTest {

	@Autowired
	private MovieRepository repository;

	@Test
	public void genericTest() {
		repository.deleteAll();

		// save a couple of movies
		repository.save(new Movie("Evita", CategoryEnum.MUSICAL.getId(), 852246000L, "Madonna"));
		repository.save(new Movie("The Matrix", CategoryEnum.SCIFI.getId(),929052000L, " Keanu Reeves"));

		//generic assert
		Assert.assertEquals(2, repository.findAll().size());
		Assert.assertEquals("Madonna", repository.findByTitle("Evita").getMainActor());
		Assert.assertEquals(1, repository.findByCategory(CategoryEnum.SCIFI.getId()).size());
	}

	@Test
	public void filterTest() {
		repository.deleteAll();

		// save a couple of movies
		repository.save(new Movie("Evita", CategoryEnum.MUSICAL.getId(), 852246000L, "Madonna"));
		repository.save(new Movie("The Matrix", CategoryEnum.SCIFI.getId(),929052000L, " Keanu Reeves"));

		Assert.assertEquals(1,
				repository.findByTitleContainsIgnoreCaseAndMainActorContainsIgnoreCaseAndCategoryIsAndReleaseDateBetween
						("Evita", "Madonna", CategoryEnum.MUSICAL.getId(), 852245000L, System.currentTimeMillis()).size());
		Assert.assertEquals(1,
				repository.findByTitleContainsIgnoreCaseAndMainActorContainsIgnoreCaseAndCategoryIsAndReleaseDateBetween
						("Evita", "", CategoryEnum.MUSICAL.getId(), 852245000L, System.currentTimeMillis()).size());
		Assert.assertEquals(1,
				repository.findByTitleContainsIgnoreCaseAndMainActorContainsIgnoreCaseAndReleaseDateBetween
						("Evita", "",852245000L, System.currentTimeMillis()).size());
	}

}
